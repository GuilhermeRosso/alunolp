unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    NetHTTPClient1: TNetHTTPClient;
    ListBox1: TListBox;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations } listaAtributo : TArray<String>;
  end;

  Type
  TPersonagem = class(TObject)
  nome, classe, listaAtributo : String;
  ataque, defesa, vida, nivel : Integer;
  end;


var
  Form1: TForm1;
  Personagem : TPersonagem;

implementation

{$R *.dfm}

uses Unit2;

procedure TForm1.Button1Click(Sender: TObject);
var
conteudo : string;
listaPersonagem : TArray<String>;
//listaAtributo : TArray<String>;
stringPersonagem : string;

begin
  ListBox1.Clear;
  conteudo := NetHTTPClient1.Get('https://venson.net.br/ws/personagens').ContentAsstring();
  listaPersonagem := conteudo.Split(['&']);
    for stringPersonagem in listaPersonagem do
      Begin
        Personagem := TPersonagem.Create;
        listaAtributo := stringPersonagem.Split([';']);
        personagem.nome := listaAtributo[0];
        personagem.classe := listaAtributo[1];
        personagem.nivel := listaAtributo[2].ToInteger;
        personagem.ataque := listaAtributo[3].ToInteger;
        personagem.defesa := listaAtributo[4].ToInteger;
        personagem.vida := listaAtributo[5].ToInteger;
        ListBox1.Items.AddObject(personagem.nome,personagem);

      End;

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Form2.ShowModal;
end;

procedure TForm1.ListBox1DblClick(Sender: TObject);
begin
  personagem := TPersonagem(ListBox1.Items.Objects[ListBox1.ItemIndex]);
  ShowMessage('Seu personagem � o ' + personagem.nome);
  ShowMessage('Seu personagem � ' + personagem.classe);
  ShowMessage('Seu personagem � nivel ' + personagem.nivel.ToString);
  ShowMessage('Seu personagem tem ' + personagem.ataque.ToString + ' de ataque');
  ShowMessage('Seu personagem tem ' + personagem.defesa.ToString + ' de defesa');
  ShowMessage('Seu personagem tem ' + personagem.vida.ToString + ' de vida');
end;

end.

