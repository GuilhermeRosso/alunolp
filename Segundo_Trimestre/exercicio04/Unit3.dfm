object DataModule3: TDataModule3
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 430
  Width = 763
  object FDConnection1: TFDConnection
    Params.Strings = (
      'User_Name=root'
      'Database=pokemon'
      'DriverID=MySQL')
    Connected = True
    Left = 185
    Top = 128
  end
  object FDQueryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from pokemon')
    Left = 272
    Top = 128
  end
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 
      'C:\EasyPHP-Devserver-17\eds-binaries\dbserver\mysql5717x86x19051' +
      '3184646\lib\libmysql.dll'
    Left = 72
    Top = 128
  end
  object DataSource1: TDataSource
    DataSet = FDQueryPokemon
    Left = 336
    Top = 128
  end
  object FDQueryTreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from treinador')
    Left = 272
    Top = 184
  end
  object DataSource2: TDataSource
    DataSet = FDQueryTreinador
    Left = 336
    Top = 184
  end
end
