object Animal_principal: TAnimal_principal
  Left = 306
  Top = 123
  Width = 594
  Height = 282
  Caption = 'Trabalho Trimestral - 352 - Animais'
  Color = clSkyBlue
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'DejaVu Sans Condensed'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 18
  object List_Animais: TListBox
    Left = 8
    Top = 8
    Width = 205
    Height = 213
    ItemHeight = 18
    Items.Strings = (
      'Nome:'
      ''
      'G'#234'nero:'
      ''
      'Sexo:'
      ''
      'Tempo de vida:')
    TabOrder = 0
  end
  object Ed_tempovida: TEdit
    Left = 227
    Top = 121
    Width = 153
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -15
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 1
    Text = 'Tempo de vida'
    OnClick = Ed_tempovidaClick
  end
  object Bt_inserir: TButton
    Left = 226
    Top = 152
    Width = 71
    Height = 23
    Caption = 'Inserir'
    TabOrder = 2
    OnClick = Bt_inserirClick
  end
  object Bt_remover: TButton
    Left = 226
    Top = 187
    Width = 71
    Height = 23
    Caption = 'Remover'
    TabOrder = 3
    OnClick = Bt_removerClick
  end
  object Bt_at: TButton
    Left = 306
    Top = 158
    Width = 71
    Height = 23
    Caption = 'Atualizar'
    TabOrder = 4
    OnClick = Bt_atClick
  end
  object Bt_save: TButton
    Left = 306
    Top = 193
    Width = 71
    Height = 23
    Caption = 'Salvar'
    TabOrder = 5
    OnClick = Bt_saveClick
  end
  object Ed_nome: TEdit
    Left = 227
    Top = 8
    Width = 153
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -15
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 6
    Text = 'Nome do Animal'
    OnClick = Ed_nomeClick
  end
  object Ed_esp: TEdit
    Left = 227
    Top = 45
    Width = 153
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -15
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 7
    Text = 'Especie do Animal'
    OnClick = Ed_espClick
  end
  object Ed_sexo: TEdit
    Left = 227
    Top = 83
    Width = 153
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -15
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 8
    Text = 'Sexo do animal'
    OnClick = Ed_sexoClick
  end
  object Bt_carregar: TButton
    Left = 434
    Top = 95
    Width = 71
    Height = 24
    Caption = 'Carregar'
    TabOrder = 9
    OnClick = Bt_carregarClick
  end
  object Memo1: TMemo
    Left = 392
    Top = 8
    Width = 161
    Height = 81
    TabOrder = 10
  end
end
